const axios = require("axios");
const util = require("../../util/");
const apiURL = "https://api.open5e.com/";
const MessageEmbed = require("discord.js").MessageEmbed;
const diceCloudWSSURL = "wss://dicecloud.com/websocket";
var categories = {};

function updateCategories() {
  axios
    .get(apiURL)
    .then((res) => {
      for (let category in res.data) {
        axios.get(res.data[category]).then((res) => {
          categories[category] = res.data;
        });
      }
      return categories;
    })
    .catch((err) => {
      console.log(err);
      util.logger.warning("Could not read categories from open5e...");
      return null;
    });
}

function shouldPickRandomItem(value) {
  if (parseInt(value, 10).toString() === value) {
    return true;
  }
  return false;
}

function getRandomItem(category) {
  category = categories[category].results;
  drop = category[Math.floor(Math.random() * category.length)];
  return drop;
}

function doesCategoryExist(category) {
  return categories[category];
}

function searchCategory(category, query) {
  let id = category.results.findIndex(
    (item) =>
      item.name &&
      (item.name == query ||
        item.slug == query ||
        item.slug.includes(query) ||
        item.slug.includes(query))
  );
  if (id) {
    return category.results[id];
  } else {
    return false;
  }
}

text_truncate = function (str, length, ending) {
  if (length == null) {
    length = 100;
  }
  if (ending == null) {
    ending = "...";
  }
  if (str.length > length) {
    return str.substring(0, length - ending.length) + ending;
  } else {
    return str;
  }
};

function getItemEmbed(drop) {
  let embed = new MessageEmbed().setTitle(drop.name).setTimestamp(); //Create a new embed with the items info and icon

  for (let property of Object.keys(drop)) {
    if (isInvalidProperty(property)) {
      continue;
    }

    if (property == "desc") {
      title = "Description";
    } else {
      title = property.replace(/([A-Z])/g, " $1"); //Remove special characters from the property key
      title = title.replace("_", " ");
      title = title.charAt(0).toUpperCase() + title.slice(1); //Generate a pretty title for the embed from property key
    }

    if (typeof property == "string") {
      if (property == "desc") {
        embed.addField(title, text_truncate(`${drop[property]}`, 1024), false); //Add field to the embed with title.
      } else {
        embed.addField(title, text_truncate(`${drop[property]}`, 1024), true); //Add field to the embed with title.
      }
    }
  }

  return embed;
}

function isInvalidProperty(property) {
  return (
    property == "name" ||
    property == "document__title" ||
    property == "document__slug" ||
    property == "document__license_url" ||
    property == "slug" ||
    property == "material" ||
    property == "archetype" ||
    property == "circles" ||
    property == "higher_level"
  );
}

updateCategories();
module.exports = {
  commands: {
    dnd: {
      name: "dnd",
      help: "Get information from opendnd5",
      parameters: {
        params: ["(category)"],
      },
      main: (bot, db, msg) => {
        let args = util.args.parse(msg)["args"];
        if (doesCategoryExist(args[0])) {
          if (!args[1]) args[1] = "1"; //If there is no item or quantity specified then set the quantity to 1

          if (shouldPickRandomItem(args[1])) {
            for (let i = 0; i < args[1]; i++) {
              msg.channel.send(getItemEmbed(getRandomItem(args[0])));
            }
          } else if (args[1]) {
            let drop = searchCategory(categories[args[0]], args[1]);
            if (drop) {
              msg.channel.send(getItemEmbed(drop));
            } else {
              msg.channel.send(
                `Could not find any matches for: ${args[1]} in list ${args[0]}`
              );
            }
          }
        } else if (!args[0]) {
          msg.channel.send(`Please Choose a list: spells, weapons`);
        } else {
          msg.channel.send(`Could not find the list: ${args[0]}`);
        }
      },
    },
  },
  events: {},
};
